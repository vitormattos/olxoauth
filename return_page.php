<?php

use ApiClient\Client;

session_start();
require 'vendor/autoload.php';

$client = new Client('olx_oauth_secrets.json');

if (!isset($_GET['code'])) {
    echo '<a href = '.$client->createAuthUrl().'>Autentica</a>';
} else {
    $_SESSION['olx_access_token'] = $client->authenticate($_GET['code']);
    header('Location: http://'.$_SERVER['HTTP_HOST'].'/');
}
