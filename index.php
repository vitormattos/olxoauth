<?php

use ApiClient\Client;

session_start();
require 'vendor/autoload.php';

$client = new Client('olx_oauth_secrets.json');
if (isset($_SESSION['olx_access_token']) && $_SESSION['olx_access_token']) {
    $dadosUsuario = $client->call('basic_user_info', json_encode(array('access_token' => $_SESSION['olx_access_token'])));
    echo '<h1>Dados do Usuario</h1>';
    var_dump(json_decode($dadosUsuario['body']));
} else {
    echo '<a href = '.$client->createAuthUrl().'>Autentica</a>';
}
